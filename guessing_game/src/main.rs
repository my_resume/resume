//! Проект включает себя изменённый исходный код, содержащийся в HTML-версии книги ["Язык программирования Rust"](https://doc.rust-lang.ru/book/)
//! от авторов Steve Klabnik, Carol Nichols и участников сообщества Rust, лицензированный под [the Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0), в соответствии с [COPYRIGHT](https://github.com/rust-lang/book/blob/main/COPYRIGHT).
//!
//! The Rust Project is copyright 2010, The Rust Project Developers.
//!
//! Copyright © 2021 Александр Кубраков.
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.
use guessing_game::Guess;
use rand::Rng;
use std::cmp::Ordering;
use std::io;
fn main() {
    println!("Угадай число!");

    let secret_number = Guess::new(rand::thread_rng().gen_range(1..101)).unwrap();
    loop {
        println!("Введите число от 1 до 100:");

        let mut guess = String::new();

        io::stdin().read_line(&mut guess).expect("Ошибка ввода!");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                eprintln!("Это было не число или отрицательное число!");
                continue;
            }
        };
        let guess = match Guess::new(guess) {
            Ok(num) => num,
            Err(err) => {
                eprintln!("Ошибка создания типа Guess: {}", err);
                continue;
            }
        };
        match guess.value().cmp(&secret_number.value()) {
            Ordering::Less => println!("Слишком малое!"),
            Ordering::Greater => println!("Слишком большое!"),
            Ordering::Equal => {
                println!("В точку!");
                break;
            }
        }
    }
}
