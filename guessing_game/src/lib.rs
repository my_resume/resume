//! Классическая программа для начинающих: угадывание числа.
//!
//! Программа генерирует случайное целое число от 1 до 100. Затем она предлагает игроку ввести и отгадать число. Если оно больше или меньше предложенного игроком, то программа сообщит об этом. Если игрок угадал число, то программа выведет поздравление и завершится.
//!
//! Программа реализована с использованием исходного кода из [главы 2](https://doc.rust-lang.ru/book/ch02-00-guessing-game-tutorial.html) и [страницы](https://doc.rust-lang.ru/book/ch09-03-to-panic-or-not-to-panic.html) [главы 9](https://doc.rust-lang.ru/book/ch09-00-error-handling.html) [книги](https://doc.rust-lang.ru/book/) с внесёнными изменениями:
//! * паника метода `new` типа данных `Guess` заменена на обрабатываемую ошибку;
//! * запись сообщений ошибок в поток ошибок с помощью `eprintln!` вместо стандартного потока вывода;
//! * тексты сообщений на русском языке;
//! * добавлены комментарии к документации.
//!
//! Проект включает себя изменённый исходный код, содержащийся в HTML-версии книги ["Язык программирования Rust"](https://doc.rust-lang.ru/book/)
//! от авторов Steve Klabnik, Carol Nichols и участников сообщества Rust, лицензированный под [the Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0), в соответствии с [COPYRIGHT](https://github.com/rust-lang/book/blob/main/COPYRIGHT).
//!
//! The Rust Project is copyright 2010, The Rust Project Developers.
//!
//! Copyright © 2021 Александр Кубраков.
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.

/// Тип данных для угадываемого числа.
pub struct Guess {
    value: u32,
}

impl Guess {
    /// Создание типа с заданными ограничениями.
    ///
    /// # Panics
    ///
    /// Функция `new` паникует, если `value` меньше 1 или больше 100.
    pub fn new(value: u32) -> Result<Guess, &'static str> {
        if !(1..=100).contains(&value) {
            return Err("Угадываемое число должно быть от 1 до 100!");
        }
        Ok(Guess { value })
    }
    /// Получение значения числа.
    pub fn value(&self) -> u32 {
        self.value
    }
}
