//! Проект включает себя изменённый исходный код, содержащийся в HTML-версии книги ["Язык программирования Rust"](https://doc.rust-lang.ru/book/)
//! от авторов Steve Klabnik, Carol Nichols и участников сообщества Rust, лицензированный под [the Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0), в соответствии с [COPYRIGHT](https://github.com/rust-lang/book/blob/main/COPYRIGHT).
//!
//! The Rust Project is copyright 2010, The Rust Project Developers.
//!
//! Copyright © 2021 Александр Кубраков.
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.
use blog::DraftPost;
use blog::Rejected; //Типаж для `PendingReview` и `PrePost`.
fn main() {
    let mut post = DraftPost::new(String::from("Начальные данные.")); //Создание черновика с типом `DraftPost`.

    post.add_text(" Добавленные данные."); //Ввод данных в черновик.

    let post = post.request_review(); //Запрос рецензии публикации, который изменяет тип данных с `DraftPost` на `PendingReview`.
    let mut post = post.reject(); //Отклонение `PendingReview` и возврат к типу данных `DraftPost`.
    post.add_text(" Новые добавленные данные."); //Добавление данных в `DraftPost`.
    let post = post.request_review(); //Запрос рецензии публикации, который изменяет тип данных с `DraftPost` на `PendingReview`.
    let post = post.approve(); //Первое одобрение публикации, которое изменяет тип данных с `PendingReview` на `PrePost`.
    let mut post = post.reject(); //Отклонение `PrePost` и возврат к типу данных `DraftPost`.
    post.add_text(" Финальные данные."); //Добавление данных в `DraftPost`.
    let post = post.request_review();
    let post = post.approve();
    let post = post.approve(); //Второе (финальное) одобрение публикации, которое изменяет тип данных с `PrePost` на `Post`.
                               // Сравнение ожидаемых данных с реальными.
    assert_eq!(
        "Начальные данные. Добавленные данные. Новые добавленные данные. Финальные данные.",
        post.content() //Запрос данных из `Post`.
    );
}
