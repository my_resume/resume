//! Процесс создания поста в блоге.
//!
//! Функциональность блога выглядит так:
//! * При создании поста, сначала методом `new` создаётся черновик `DraftPost`, в который можно добавлять данные методом `add_text`.
//! * Когда черновик готов, с помощью метода `request_review` тип данных меняется на ожидающую рецензирование публикацию `PendingReviewPost`, лишённую возможности добавлять данные.
//! * После утверждения методом `approve` тип `PendingReviewPost` меняется на `Post`.
//! * Только сообщения блога с типом `Post` возвращают свой контент пользователям.
//!
//! Программа реализована с использованием исходного кода со [страницы](https://doc.rust-lang.ru/book/ch17-03-oo-design-patterns.html) [главы 17](https://doc.rust-lang.ru/book/ch17-00-oop.html) [книги](https://doc.rust-lang.ru/book/) с внесёнными изменениями:
//! * добавлен метод `reject`, который изменяет состояние публикации из `PendingReviewPost` (и введённого для выполнения следующего пункта `PrePost`) обратно в `Draft`.
//! * требуются два вызова метода `approve` прежде чем тип данных станет `Post`;
//! * метод `new` был убран у типа `Post` и добавлен типу `DraftPost`, также к этому методу был добавлен аргумент `content`;
//! * тексты сообщений на русском языке;
//! * добавлены комментарии к документации.
//!
//! Проект включает себя изменённый исходный код, содержащийся в HTML-версии книги ["Язык программирования Rust"](https://doc.rust-lang.ru/book/)
//! от авторов Steve Klabnik, Carol Nichols и участников сообщества Rust, лицензированный под [the Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0), в соответствии с [COPYRIGHT](https://github.com/rust-lang/book/blob/main/COPYRIGHT).
//!
//! The Rust Project is copyright 2010, The Rust Project Developers.
//!
//! Copyright © 2021 Александр Кубраков.
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.

/// Пост, прошедший рецензирование и два подтверждения.
pub struct Post {
    content: String,
}

impl Post {
    /// Получение данных из поста.
    pub fn content(&self) -> &str {
        &self.content
    }
}
/// Черновик поста.
pub struct DraftPost {
    content: String,
}
impl DraftPost {
    /// Создание черновика.
    ///
    /// Переменная `content`  - это введённые в черновик данные.
    pub fn new(content: String) -> DraftPost {
        DraftPost { content }
    }
    /// Добавление данных в черновик.
    ///
    /// Переменная `text`  - это добавляемые в черновик данные.
    pub fn add_text(&mut self, text: &str) {
        self.content.push_str(text);
    }
    /// Запрос рецензии публикации.
    pub fn request_review(self) -> PendingReviewPost {
        PendingReviewPost {
            content: self.content,
        }
    }
}
/// Ожидающая рецензирование публикация, лишённая возможности добавлять данные.
pub struct PendingReviewPost {
    content: String,
}

impl PendingReviewPost {
    /// Предварительное одобрение публикации.
    pub fn approve(self) -> PrePost {
        PrePost {
            content: self.content,
        }
    }
}
impl Rejected for PendingReviewPost {
    fn reject(self) -> DraftPost {
        DraftPost {
            content: self.content,
        }
    }
}
/// Предварительно одобренный пост.
pub struct PrePost {
    content: String,
}
impl PrePost {
    /// Окончательное одобрение публикации.
    pub fn approve(self) -> Post {
        Post {
            content: self.content,
        }
    }
}
impl Rejected for PrePost {
    fn reject(self) -> DraftPost {
        DraftPost {
            content: self.content,
        }
    }
}
/// Типаж для типов публикации, которые могут быть отклонены.
pub trait Rejected {
    /// Отклонение публикации и изменение типа данных на черновик `DraftPost`.
    fn reject(self) -> DraftPost;
}
