//! Проект с вводом/выводом (I/O) - консольное приложение, которое ищет данные строки поиска в указанном файле.
//!
//! Приложение принимает на вход вторую (имя файла) и третью (строка для поиска) переменные командной строки и переменные окружения. Если на входе установлена переменная окружения `CASE_INSENSITIVE`, то поиск данных строки в файле ведётся без учёта регистра.
//!
//! Программа реализована с использованием исходного кода из [главы 12](https://doc.rust-lang.ru/book/ch12-00-an-io-project.html) и [страницы](https://doc.rust-lang.ru/book/ch13-03-improving-our-io-project.html) [главы 13](https://doc.rust-lang.ru/book/ch13-00-functional-features.html) [книги](https://doc.rust-lang.ru/book) с внесёнными изменениями:
//! * убраны ненужные ключевые слова `pub`;
//! * аргумент `args` метода `new` типа `Config` внесён внутрь этого метода;
//! * тексты сообщений на русском языке;
//! * добавлены комментарии к документации.
//!
//! Проект включает себя изменённый исходный код, содержащийся в HTML-версии книги ["Язык программирования Rust"](https://doc.rust-lang.ru/book/)
//! от авторов Steve Klabnik, Carol Nichols и участников сообщества Rust, лицензированный под [the Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0), в соответствии с [COPYRIGHT](https://github.com/rust-lang/book/blob/main/COPYRIGHT).
//!
//! The Rust Project is copyright 2010, The Rust Project Developers.
//!
//! Copyright © 2021 Александр Кубраков.
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.

use std::env;
use std::error::Error;
use std::fs;
/// Структура конфигурации, хранящая введённые пользователем данные.
pub struct Config {
    query: String,
    filename: String,
    case_sensitive: bool,
}

impl Config {
    /// Создание структуры конфигурации.
    ///
    /// Функция `new` принимает на вход итератор аргументов `env::args()`, содержащий вторую (имя файла) и третью (строка для поиска) переменные командной строки  и, опционально, переменную окружения `CASE_INSENSITIVE`. Значение переменных командной строки и факт наличия или отсутствия переменной окружения `CASE_INSENSITIVE` (игнорируя значение `CASE_INSENSITIVE`) записывается в `Config`.
    ///
    /// # Errors
    ///
    /// Функция `new` возвращает ошибку, если не удалось получить вторую (имя файла) или третью (строка для поиска) переменную командной строки из входного итератора.
    pub fn new() -> Result<Config, &'static str> {
        let mut args = env::args();
        args.next();
        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("Не удалось получить строку для поиска!"),
        };

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Не удалось получить имя файла!"),
        };
        let case_sensitive = env::var("CASE_INSENSITIVE").is_err();
        Ok(Config {
            query,
            filename,
            case_sensitive,
        })
    }
}
/// Внутренняя логика.
///
/// Переменная `config` - структура конфигурации для поиска.
///
/// В зависимости от наличия переменной окружения `CASE_INSENSITIVE` при создании входной структуры конфигурации, выполняется поиск с учётом или без учёта регистра.
///
/// # Errors
///
/// Функция `run` возвращает ошибку, если не удалось прочитать имя файла `filename` из структуры конфигурации `config`.
pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;

    let results = if config.case_sensitive {
        search(&config.query, &contents)
    } else {
        search_case_insensitive(&config.query, &contents)
    };

    for line in results {
        println!("{}", line);
    }

    Ok(())
}
// Поиск с учётом регистра.
fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents
        .lines()
        .filter(|line| line.contains(query))
        .collect()
}
// Поиск без учёта регистра.
fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    contents
        .lines()
        .filter(|line| line.to_lowercase().contains(&query))
        .collect()
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}
