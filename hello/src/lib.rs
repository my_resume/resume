//! Многопоточный веб-сервер.
//!
//!  Сервер открывает страницу “hello”, если запрос начинается с "GET / HTTP/1.1\r\n", предварительно ждёт 5 секунд, если запрос начинается с "GET /sleep HTTP/1.1\r\n" или открывает страницу "404" в остальных случаях.
//!
//! Финальный проект [книги](https://doc.rust-lang.ru/book/), представленный в [главе 20](https://doc.rust-lang.ru/book/ch20-00-final-project-a-web-server.html) с внесёнными изменениями:
//! * функция `handle_connection` перенесена в файл lib.rs;
//! * методы `unwrap` заменены на `expect`;
//! * тексты сообщений на русском языке;
//! * добавлены комментарии к документации.
//!
//! Проект включает себя изменённый исходный код, содержащийся в HTML-версии книги ["Язык программирования Rust"](https://doc.rust-lang.ru/book/)
//! от авторов Steve Klabnik, Carol Nichols и участников сообщества Rust, лицензированный под [the Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0), в соответствии с [COPYRIGHT](https://github.com/rust-lang/book/blob/main/COPYRIGHT).
//!
//! The Rust Project is copyright 2010, The Rust Project Developers.
//!
//! Copyright © 2021 Александр Кубраков.
//!
//! Licensed under the Apache License, Version 2.0 (the "License");
//! you may not use this file except in compliance with the License.
//! You may obtain a copy of the License at
//!
//! <https://www.apache.org/licenses/LICENSE-2.0>
//!
//! Unless required by applicable law or agreed to in writing, software
//! distributed under the License is distributed on an "AS IS" BASIS,
//! WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//! See the License for the specific language governing permissions and
//! limitations under the License.

use std::fs;
use std::io::prelude::*;
use std::net::TcpStream;
use std::sync::mpsc;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::time::Duration;
/// Пул потоков является группой заранее порождённых потоков, ожидающих в пуле и готовых выполнить задачу. Когда программа получает новую задачу, она
/// назначает задачу одному из потоков в пуле и этот поток будет обрабатывать задачу. Остальные потоки в пуле доступны для обработки любых других задач,
/// возникающих во время обработки первого потока. Когда первый поток завершает обработку своей задачи, он возвращается в пул свободных потоков,
/// готовых обработать новую задачу. Пул потоков позволяет обрабатывать соединения одновременно, увеличивая пропускную способность сервера.
pub struct ThreadPool {
    workers: Vec<Worker>,
    sender: mpsc::Sender<Message>,
}

type Job = Box<dyn FnOnce() + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate,
}

impl ThreadPool {
    /// Создание нового пула потоков.
    ///
    /// Переменная `size`  - это количество потоков в пуле.
    ///
    /// # Panics
    ///
    /// Функция `new` паникует, если `size` равно нулю.
    pub fn new(size: usize) -> ThreadPool {
        assert!(size > 0);

        let (sender, receiver) = mpsc::channel();

        let receiver = Arc::new(Mutex::new(receiver));

        let mut workers = Vec::with_capacity(size);

        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver)));
        }

        ThreadPool { workers, sender }
    }
    /// Отправка работы для потоков в пуле.
    ///
    /// Замыкание `f`  - это работа для потока в пуле.
    pub fn execute<F>(&self, f: F)
    where
        F: FnOnce() + Send + 'static,
    {
        let job = Box::new(f);

        self.sender
            .send(Message::NewJob(job))
            .expect("Не удалось отправить сообщение с работой потоку.");
    }
}

impl Drop for ThreadPool {
    /// Отключение пула потоков.
    fn drop(&mut self) {
        println!("Отправка сообщения об остановке всем работникам.");

        for _ in &self.workers {
            self.sender
                .send(Message::Terminate)
                .expect("Не удалось отправить сообщение об остановке.");
        }

        println!("Отключение всех работников.");

        for worker in &mut self.workers {
            println!("Отключение работника {}.", worker.id);

            if let Some(thread) = worker.thread.take() {
                thread.join().expect("Объединение потоков не удалось!");
            }
        }
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        let thread = thread::spawn(move || loop {
            let message = receiver
                .lock()
                .expect("Блокировка мьютекса не удалась.")
                .recv()
                .expect("Извлечение данных из мьютекса не удалось.");

            match message {
                Message::NewJob(job) => {
                    println!("Работник {} получил работу; выполняю.", id);

                    job();
                }
                Message::Terminate => {
                    println!("Работнику {} было сказано остановиться.", id);

                    break;
                }
            }
        });

        Worker {
            id,
            thread: Some(thread),
        }
    }
}
/// Управление соединением.
///
/// Алгоритм, отправляемый на выполнение потоку, если установлено соединение.
///
/// Переменная `stream` содержит запрос данных от клиента.
///
/// # Panics
///
/// Функция `handle_connection` паникует если:
/// * чтение данных запроса в буфер не удалось;
/// * чтение запрошенного клиентом файла не удалось;
/// * запись запрошенных клиентом данных в поток не удалась
/// * освобождение потока вызвало панику.
pub fn handle_connection(mut stream: TcpStream) {
    let mut buffer = [0; 1024];
    stream
        .read(&mut buffer)
        .expect("Чтение в буфер не удалось!");

    let get = b"GET / HTTP/1.1\r\n";
    let sleep = b"GET /sleep HTTP/1.1\r\n";

    let (status_line, filename) = if buffer.starts_with(get) {
        ("HTTP/1.1 200 OK", "hello.html")
    } else if buffer.starts_with(sleep) {
        thread::sleep(Duration::from_secs(5));
        ("HTTP/1.1 200 OK", "hello.html")
    } else {
        ("HTTP/1.1 404 NOT FOUND", "404.html")
    };

    let contents =
        fs::read_to_string(filename).expect(&format!("Чтение файла {} не удалось!", filename));

    let response = format!(
        "{}\r\nContent-Length: {}\r\n\r\n{}",
        status_line,
        contents.len(),
        contents
    );

    stream
        .write(response.as_bytes())
        .expect(&format!("Запись данных {} в поток не удалась!", response));
    stream.flush().expect("Освобождение потока вызвало панику!");
}
